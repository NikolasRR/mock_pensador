import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import userRepo from "../repositories/userRepos.js";
import { EditBody, LogInBody, UserBody } from "../types/userTypes.js";

async function create(data: UserBody) {
  const user = await userRepo.getByEmail(data.email);
  if (user !== null) throw { type: "conflict" };

  const hashedPassword = bcrypt.hashSync(data.password, 10);
  data.password = hashedPassword;

  await userRepo.create(data);
}

async function getById(id: number) {
  return await userRepo.getById(id);
}

async function logIn(data: LogInBody) {
  const user = await userRepo.getByEmail(data.email);
  if (user === null) throw { type: "not found" };

  const passwordIsCorrect = bcrypt.compareSync(data.password, user.password);
  if (!passwordIsCorrect) throw { type: "unauthorized" };

  return generateToken(user.id);
}

async function updateUser(data: EditBody, id: number) {
  if (data.password)  data.password = bcrypt.hashSync(data.password, 10);
  await userRepo.updateByEmail(data, id);
}

async function deleteUser(id: number) {
  await userRepo.deleteById(id);
}

function generateToken(id: number) {
  const TwentyFourHours = '24h';
  const token = jwt.sign({ id: id }, process.env.JWT_SECRET, { expiresIn: TwentyFourHours });
  return token;
}

const userService = {
  create,
  logIn,
  updateUser,
  getById,
  deleteUser
}

export default userService;
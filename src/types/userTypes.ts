import { User } from "@prisma/client";

export interface UserBody extends Omit<User, "id" | "createdAt"> {};

export interface LogInBody extends Omit<User, "id" | "createdAt" | "name"> {};

export interface EditBody extends Partial<Omit<User, "id" | "createdAt">> {};
import { Router } from "express";

import controllers from "../controllers/userControllers.js";
import validateToken from "../middlewares/tokenValidator.js";
import middleware from "../middlewares/userMiddlewares.js";

const userRouter = Router();

userRouter
  .post("/sign-up", middleware.validateCreationData, controllers.createUser)
  .post("/sign-in", middleware.validateLogInData, controllers.logUserIn)
  .put("/user", validateToken, middleware.validateEditData, controllers.editUser)
  .get("/user", validateToken, controllers.getUser)
  .delete("/user", validateToken, controllers.deleteUser);

export default userRouter;
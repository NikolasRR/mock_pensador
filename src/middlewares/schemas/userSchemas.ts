import Joi from "joi";

import { EditBody, LogInBody, UserBody } from "../../types/userTypes.js";

export const userSchema = Joi.object<UserBody>({
  name: Joi.string().max(40).required(),
  email: Joi.string().email().required(),
  password: Joi.string().max(10).required()
})

export const loginSchema = Joi.object<LogInBody>({
  email: Joi.string().email().required(),
  password: Joi.string().max(40).required()
})

export const editSchema = Joi.object<EditBody>({
  name: Joi.string().max(40),
  email: Joi.string().email(),
  password: Joi.string().max(10)
})
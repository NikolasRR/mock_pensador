import prisma from "../database/db.js";

import { EditBody, UserBody } from "../types/userTypes.js";

async function create(user: UserBody) {
  await prisma.user.create({
    data: user
  });  
}

async function getByEmail(email: string) {
  return await prisma.user.findUnique({
    where: {
      email: email
    }
  });
}

async function getById(id: number) {
  return await prisma.user.findUnique({
    where: {
      id: id
    }
  });
}

async function updateByEmail(data: EditBody, id: number) {
  return await prisma.user.update({
    where: {
      id: id
    },
    data: data
  });
}

async function deleteById(id: number) {
  await prisma.user.delete({
    where: {
      id: id
    }
  });
}

const userRepo = {
  create,
  getByEmail,
  getById,
  updateByEmail,
  deleteById
}

export default userRepo;
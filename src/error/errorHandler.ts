import { NextFunction, Request, Response } from "express";

type error = {
  type: string,
  message: string
};

export default async function errorHandler(error: error, req: Request, res: Response, next: NextFunction) {
  let code: number;

  switch (error.type) {
    case "bad request":
      code = 400;
      break;
    case "unauthorized":
      code = 401;
      error.message = "senha errada";
      break;
    case "subscription expired":
      code = 401;
      error.message = "sua assinatura expirou";
      break;
    case "double login":
      code = 401;
      error.message = "double login";
      break;
    case "not found":
      code = 404;
      error.message = "email não encontrado";
      break;
    case "token":
      code = 422;
      error.message = "token missing, expired or unvalid";
      break;
    default:
      code = 500;
      break;
  }
  res.status(code).send(error.message);
}
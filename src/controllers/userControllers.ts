import { Request, Response } from "express";

import userService from "../services/userServices.js";
import { EditBody, LogInBody, UserBody } from "../types/userTypes.js";

async function createUser(req: Request, res: Response) {
  const body = req.body as UserBody;
  await userService.create(body);

  res.sendStatus(201);
}

async function editUser(req: Request, res: Response) {
  const body = req.body as EditBody;
  const id: number = res.locals.userId;
  await userService.updateUser(body, id);

  res.sendStatus(200);
}

async function deleteUser(req: Request, res: Response) {
  const id: number = res.locals.userId;
  await userService.deleteUser(id);

  res.sendStatus(200);
}

async function getUser(req: Request, res: Response) {
  const id: number = res.locals.userId;
  const user = await userService.getById(id);

  delete user.password;

  res.send(user);
}

async function logUserIn(req: Request, res: Response) {
  const body = req.body as LogInBody;
  const token = await userService.logIn(body);

  res.send({ token: token });
}

const controllers = {
  createUser,
  editUser,
  deleteUser,
  getUser,
  logUserIn
}

export default controllers;